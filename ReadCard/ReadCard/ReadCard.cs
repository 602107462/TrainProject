﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Net;

namespace ReadCard
{
    public partial class ReadCard : Form
    {
        private UdpClient udpclient;
        public ReadCard(LoginForm loginform)
        {
            InitializeComponent();
        }

        private void buttonListen_Click(object sender, EventArgs e)
        {
            Control.CheckForIllegalCrossThreadCalls = false;
            if (buttonListen.Text == "监听")
            {
                string listenPort = textBoxListenPort.Text;
                udpclient = new UdpClient(Convert.ToInt32(listenPort));
                udpclient.BeginReceive(new AsyncCallback(ReciveCallback), null);
                buttonListen.Text = "停止监听";
            }
            else
            {
                 //IPEndPoint endpoint = null;
                 //udpclient.EndReceive(null, ref endpoint);
                udpclient.Close();
                buttonListen.Text = "监听";
            }
        }


        private void ReciveCallback(IAsyncResult asyncResult)
        {
            if (asyncResult.IsCompleted)
            {
                try
                {
                    IPEndPoint endpoint = null;
                    byte[] b = udpclient.EndReceive(asyncResult, ref endpoint);
                    udpclient.BeginReceive(new AsyncCallback(ReciveCallback), null);
                    string reciveBytes = string.Empty;
                    for (int i = 0; i < b.Length; i++)
                    {
                        reciveBytes += b[i].ToString("X2") + " ";
                    }
                    textBoxMsgs.Text += "Recive:" + reciveBytes + "\r\n";
                }
                catch (Exception e)
                {

                }
               
            }
        }

        private void buttonSend_Click(object sender, EventArgs e)
        {
            string sss = DateTime.Now.ToString("yy");
            string[] strs = textBoxCmd.Text.Split(' ');
            textBoxMsgs.Text += "Send:" + textBoxCmd.Text+"\r\n";
            byte[] sendBytes = new byte[strs.Length];
            for (int i = 0; i < strs.Length; i++)
            {
                sendBytes[i] = (byte)Convert.ToInt32(strs[i],16);
            }
             IPEndPoint remoteEP = new IPEndPoint(IPAddress.Parse(textBoxCardIp.Text),Convert.ToInt32(textBoxCardPort.Text));
            udpclient.Send(sendBytes, sendBytes.Length,remoteEP);
        }

        private void ClearScreen_Click(object sender, EventArgs e)
        {
            textBoxMsgs.Text = "";
        }
    }
}
