﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadCard.Interface
{
    interface IReadCardSender
    {
        byte[] SendMsg();
    }
}
