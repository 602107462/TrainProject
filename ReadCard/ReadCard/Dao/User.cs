﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadCard.Dao
{
    public class User
    {
        public string Id { get; set; }
        public string UserCode { get; set; }
        public string Password { get; set; }
        public string UserName { get; set; }
        public int Source { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Userjob { get; set; }
        public string CollegeId { get; set; }
        public string DepartmentId { get; set; }
        public string ERPId { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime ModifyTime { get; set; }
        public string Creator { get; set; }
        public string Modifier { get; set; }
        public bool IsFirstLogin { get; set; }
        public bool IsValid { get; set; }
        public DateTime LastLoginTime { get; set; }
    }
}

