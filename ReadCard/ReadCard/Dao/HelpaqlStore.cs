﻿using ReadCard.Help;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadCard.Dao
{
    public class HelpsqlStore
    {
        public static DataTable GetClientComputerDt()
        {
            string sql = @"select c.ID,c.ComputerIP,c.RoomID,c.ComputerMAC,c.ComputerPort,c.UserName,c.Password,CAST(c.StartTime as DateTime)as starttime,CAST(c.CloseTime as DateTime)as closetime,c.Status,c.CreateTime,c.Creator,c.ModifyTime,c.Modifier,c.IsAuto,r.RoomName from tbClientComputer c left join  tbClassRoom r on c.RoomID = r.ID ";
            return HelpSql.GetDataTableFormSql(sql);
        }

        public static User GetUserByLoginName(string LoginName)
        {
            string sql = "select * from tbUser where userCode = '" + LoginName + "'";
            DataTable dt = HelpSql.GetDataTableFormSql(sql);
            User user = null;
            if (dt.Rows.Count > 0)
            {
                user = new User();
                user.Id = dt.Rows[0]["Id"] + "";
                user.UserCode = dt.Rows[0]["UserCode"] + "";
                user.Password = dt.Rows[0]["Password"] + "";
                user.UserName = dt.Rows[0]["UserName"] + "";
                user.Source = dt.Rows[0]["Source"] == DBNull.Value ? 0 : Convert.ToInt16(dt.Rows[0]["Source"]);
                user.Email = dt.Rows[0]["Email"] + "";
                user.PhoneNumber = dt.Rows[0]["PhoneNumber"] + "";
                user.Userjob = dt.Rows[0]["UserJob"] + "";
                user.CollegeId = dt.Rows[0]["CollegeId"] + "";
                user.DepartmentId = dt.Rows[0]["DepartmentId"] + "";
                user.ERPId = dt.Rows[0]["ERPID"] + "";
                user.CreateTime = (DateTime)dt.Rows[0]["CreateTime"];
                user.Creator = dt.Rows[0]["Creator"] + "";
                user.Modifier = dt.Rows[0]["Modifier"] + "";
                user.ModifyTime = (DateTime)dt.Rows[0]["ModifyTime"];
                user.IsFirstLogin = dt.Rows[0]["isFirstLogin"] + "" == "0";
                user.IsValid = dt.Rows[0]["IsValid"] + "" == "1";
                user.LastLoginTime = dt.Rows[0]["LastLoginTime"] == DBNull.Value ? new DateTime(1970, 1, 1) : (DateTime)dt.Rows[0]["LastLoginTime"];
            }
            return user;
        }
    }
}

        //public static List<ClientComputer> GetCameraInfoByDataTable(DataTable dtInfo)
        //{
        //    List<ClientComputer> list = new List<ClientComputer>();
        //    ClientComputer info = null;
        //    foreach (DataRow dr in dtInfo.Rows)
        //    {
        //        info = new ClientComputer();
        //        info.ID = dr["ID"] + "";
        //        info.RoomName = dr["RoomName"] + "";
        //        info.ComputerIP = dr["ComputerIP"] + "";
        //        info.RoomID = dr["RoomID"] + "";
        //        info.ComputerMAC = dr["ComputerMAC"] + "";
        //        info.ComputerPort = dr["ComputerPort"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ComputerPort"]);  
        //        info.UserName = dr["UserName"] + "";
        //        info.Password = dr["Password"] + "";
        //        info.StartTime = dr["StartTime"] == DBNull.Value ? new DateTime(1970, 1, 1) : (DateTime)dr["StartTime"];
        //        info.CloseTime = dr["CloseTime"] == DBNull.Value ? new DateTime(1970, 1, 1) : (DateTime)dr["CloseTime"];
        //        info.Status = dr["Status"] == DBNull.Value ? 0 : Convert.ToInt32(dr["Status"]);
        //        info.CreateTime =(DateTime)dr["CreateTime"];
        //        info.ModifyTime = (DateTime)dr["ModifyTime"];
        //        info.Creator = dr["Creator"] + "";
        //        info.Modifier = dr["Modifier"] + "";
        //        info.IsAuto = dr["IsAuto"] == DBNull.Value ? true : Convert.ToBoolean(dr["IsAuto"]);
        //        list.Add(info);
        //    }
        //    return list;

        //}