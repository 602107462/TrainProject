﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadCard.Help
{
    public class HelpSql
    {

        private HelpSql()
        { }
        public static DataTable GetDataTableFormSql(string sql)
        {
            SqlConnection conn = OpenSql();
            DataTable datatable = new DataTable();
            try
            {
                conn.Open();
                new SqlDataAdapter(sql, conn).Fill(datatable);
            }
            catch (Exception e)
            {
                HelpLog.WriteErrorLog(e);
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }
            return datatable;
        }

        private static SqlConnection OpenSql()
        {
            SqlConnection conn = new SqlConnection(HelpConstant.Instance.connStr);

            return conn;
        }


    }
}
