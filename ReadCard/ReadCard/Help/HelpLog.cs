﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadCard.Help
{
    public class HelpLog
    {
        public delegate void WriteMsg(string msg);
        public static event WriteMsg writeMsg;

        private static string LogFilePath;
        private static object objLock = new object();

        private static string GetLogFilePath()
        {
            string path = string.Empty;
            if (string.IsNullOrEmpty(LogFilePath))
            {
                path = Environment.CurrentDirectory;
            }
            if (!path.EndsWith(@"\"))
            {
                path = path + @"\";
            }
            path = path + @"logs\";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            return path;
        }

        public static void SetLogFilePath(string path)
        {
            LogFilePath = path;
        }

        public static void WriteErrorLog(Exception ex)
        {
            WriteErrorLog(ex.ToString());
            if (writeMsg != null)
                writeMsg(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + " ErrorLog ----- " + ex.Message + "\r\n");
        }

        public static void WriteErrorLog(string contents)
        {
            string logFilePath = GetLogFilePath();
            string str2 = "Log_" + DateTime.Today.ToString("yyyy_MM_dd") + ".txt";
            lock (objLock)
            {
                File.AppendAllText(logFilePath + str2, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + " ErrorLog " + contents + "\r\n");
            }
        }

        public static void WriteJobLog(string contents)
        {
            string logFilePath = GetLogFilePath();
            string str2 = "Log_" + DateTime.Today.ToString("yyyy_MM_dd") + ".txt";
            lock (objLock)
            {
                File.AppendAllText(logFilePath + str2, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + " JobLog " + contents + "\r\n");
            }
        }
    }
}
