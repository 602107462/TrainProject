﻿using CryptDll;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadCard.Help
{
    class HelpConstant
    {
        private HelpConstant()
        {
        }
        private static HelpConstant _instance = null;
        public static HelpConstant Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new HelpConstant();
                return _instance;
            }
        }

        public string loadFrame = System.Configuration.ConfigurationSettings.AppSettings["loadFrame"] + "";
        public string connStr
        {
            get
            {
                try
                {
                    string decryptStr = System.Configuration.ConfigurationSettings.AppSettings["constr"] + "";
                    decryptStr = CryptOP.DecryptStr(decryptStr);
                    return decryptStr;
                }
                catch (Exception ex)
                {
                    HelpLog.WriteErrorLog(ex);
                    return string.Empty;
                }

            }
        }
        public bool isShowCameraTitle = (System.Configuration.ConfigurationSettings.AppSettings["showCameraTitle"] + "").Equals("true", StringComparison.OrdinalIgnoreCase) ? true : false;
        public int ieRendering
        {
            get
            {

                int i = 0;
                int.TryParse(System.Configuration.ConfigurationSettings.AppSettings["showCameraTitle"] + "", out i);
                if (i > 2000)
                    return i;
                else
                    return 9000;
            }
        }

        public bool isAspectRatio
        {
            get
            {
                bool returnVal = false;
                string ratioStr = System.Configuration.ConfigurationSettings.AppSettings["isAspectRatio"] + "";
                bool.TryParse(ratioStr, out returnVal);
                return returnVal;
            }
        }
        public string title
        {
            get
            {
                string val = System.Configuration.ConfigurationSettings.AppSettings["title"];
                return val;
            }
        }
        private object[] returnVal = null;
        public object[] RollingInterval
        {
            get
            {
                if (returnVal == null)
                {
                    string intervalStr = System.Configuration.ConfigurationSettings.AppSettings["rollingInterval"];
                    string[] timesStr = intervalStr.Split(',');
                    returnVal = new object[timesStr.Length];
                    for (int i = 0; i < returnVal.Length; i++)
                    {
                        int temp = 0;
                        int.TryParse(timesStr[i], out temp);
                        returnVal[i] = temp;
                    }
                }
                return returnVal;
            }
        }

        public bool ShowCloseButton
        {
            get
            {
                bool returnval = false;
                string str = System.Configuration.ConfigurationSettings.AppSettings["showCloseButton"];
                bool.TryParse(str, out returnval);
                return returnval;
            }
        }
    }
}

