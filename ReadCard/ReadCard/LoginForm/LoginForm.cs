﻿using ReadCard.Dao;
using ReadCard.Help;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;

namespace ReadCard
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            //居中显示未完成
            InitializeComponent();
            labelTitle.Text = HelpConstant.Instance.title;
            labelTitle.Left = (this.Width - labelTitle.Width) / 2;
        }
        /// <summary>
        /// 登录用户
        /// </summary>
        public User LoginUser { get; set; }
        public ReadCard mainForm { get; set; }
        private void buttonExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        
        private void buttonLogin_Click(object sender, EventArgs e)
        {
            try
            {
                string userName = this.textBoxUser.Text.Trim();
                string password = this.textBoxPwd.Text.Trim();
                if (userName.Length == 0) throw new Exception("用户名不能为空！");
                if (password.Length == 0) throw new Exception("密码不能为空！"); 
                User user;
                string pwd = CryptDll.CryptOP.MD5Encrypt(this.textBoxPwd.Text);
                //TODO 查询数据库
                user = HelpsqlStore.GetUserByLoginName(userName);
                if (user == null) throw new Exception("该用户名不存在！");
                if (!pwd.Equals(user.Password, StringComparison.OrdinalIgnoreCase)) throw new Exception("密码错误！");
                //如果数据库查询成功
                LoginUser = user;
                ReadCard form = new ReadCard(this);
                mainForm = form;
                this.DialogResult = System.Windows.Forms.DialogResult.OK;        
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "", MessageBoxButtons.OK,  MessageBoxIcon.Error);
                return;
            }
        }

        private void LoginForm_Load(object sender, EventArgs e)
        {
            this.labelTitle.Left = this.Width / 2 - this.labelTitle.Width / 2;
            this.labelCompany.Left = this.Width / 2 - this.labelCompany.Width / 2;
        }      
    }
}
