﻿using ReadCard.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadCard.SendImplement
{
    public class SendDownLoadDeviceId : SendCardMsg, IReadCardSender
    {
        private string _DeviceId = string.Empty;
        private SendDownLoadDeviceId() { }
        public SendDownLoadDeviceId(string DeviceId)
        {
            _DeviceId = DeviceId;
        }
        protected override CardCmdType cmdType
        {
            get { return CardCmdType.DownLoadDeviceId; }
        }

        protected override byte[] CreateSendData()
        {
            string DeviceId = _DeviceId.PadLeft(8, '0');
            byte[] bytecmd = new byte[9];
            byte xor = 0;
            int position = 0;
            for (int i = 0; i < DeviceId.Length; i += 2)
            {
                byte temp = Convert.ToByte(DeviceId.Substring(i, 2));
                bytecmd[position] = temp;
                bytecmd[position + 4] = ((byte)~temp); //取反码
                xor ^= temp; //异或的结果
                position += 1;
            }
            bytecmd[bytecmd.Length - 1] = xor;
            return bytecmd;
        }

        public byte[] SendMsg()
        {
            return this.MontageByte();
        }
    }
}
