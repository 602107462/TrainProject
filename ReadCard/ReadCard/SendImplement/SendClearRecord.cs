﻿using ReadCard.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadCard.SendImplement
{
    class SendClearRecord : SendCardMsg, IReadCardSender
    {
        private ClearType _clearType;
        private SendClearRecord() { }
        public SendClearRecord(ClearType clearType)
        {
            _clearType = clearType;
        }
        protected override CardCmdType cmdType
        {
            get { return CardCmdType.ClearRecord; }
        }

        protected override byte[] CreateSendData()
        {
            return new byte[] { (byte)_clearType };
        }

        public byte[] SendMsg()
        {
            return this.MontageByte();
        }
    }
}
