﻿using ReadCard.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadCard.SendImplement
{
    /// <summary>
    /// 通讯测试命令
    /// </summary>
    public class SendTestCommunication : SendCardMsg,IReadCardSender
    {
        public SendTestCommunication() { }
        protected override byte[] CreateSendData()
        {
            return new byte[0];
        }

        protected override CardCmdType cmdType
        {
            get { return CardCmdType.CommTest; }
        }

        public byte[] SendMsg()
        {
            return this.MontageByte();
        }
    }
}
