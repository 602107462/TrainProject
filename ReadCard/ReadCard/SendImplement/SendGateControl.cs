﻿using ReadCard.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadCard.SendImplement
{
    public class SendGateControl : SendCardMsg, IReadCardSender
    {
        private GateStatus _gateStatus;
        private SendGateControl() { }
        public SendGateControl(GateStatus gateStatus) 
        {
            _gateStatus = gateStatus;
        }

        protected override CardCmdType cmdType
        {
            get { return CardCmdType.GateControl; }
        }

        protected override byte[] CreateSendData()
        {
            return new byte[] {(byte)_gateStatus };
        }

        public byte[] SendMsg()
        {
            return this.MontageByte();
        }
    }
}
