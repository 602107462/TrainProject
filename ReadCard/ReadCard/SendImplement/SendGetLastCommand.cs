﻿using ReadCard.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadCard.SendImplement
{
    public class SendGetLastCommand : SendCardMsg, IReadCardSender
    {
        public SendGetLastCommand() { }

        protected override CardCmdType cmdType
        {
            get { return CardCmdType.LastCmd; }
        }

        protected override byte[] CreateSendData()
        {
            return new byte[0];
        }

        public byte[] SendMsg()
        {
            return this.MontageByte();
        }
    }
}
