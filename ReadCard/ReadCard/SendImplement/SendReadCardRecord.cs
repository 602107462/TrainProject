﻿using ReadCard.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadCard.SendImplement
{
    public class SendReadCardRecord : SendCardMsg, IReadCardSender
    {
        private ushort _cardPosition;
        private SendReadCardRecord() { }
        public SendReadCardRecord(ushort cardPosition)
        {
            _cardPosition = cardPosition;
        }

        protected override CardCmdType cmdType
        {
            get { return CardCmdType.ReadCardRecord; }
        }

        protected override byte[] CreateSendData()
        {
            return HelpCard.Instance.SetCardPositionByte(_cardPosition);
        }

        public byte[] SendMsg()
        {
            return this.MontageByte();
        }
    }
}
