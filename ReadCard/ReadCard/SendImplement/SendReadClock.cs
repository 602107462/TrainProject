﻿using ReadCard.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadCard.SendImplement
{
    public class SendReadClock : SendCardMsg, IReadCardSender
    {
        public SendReadClock() { }
        protected override CardCmdType cmdType
        {
            get { return CardCmdType.ReadClock; }
        }

        protected override byte[] CreateSendData()
        {
            return new byte[0];
        }

        public byte[] SendMsg()
        {
            return this.MontageByte();
        }
    }
}
